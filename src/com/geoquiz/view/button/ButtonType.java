package com.geoquiz.view.button;

public enum ButtonType {
        CATEGORY,
        DIFFICULTY_LEVEL,
        GAME_MODE;

}
