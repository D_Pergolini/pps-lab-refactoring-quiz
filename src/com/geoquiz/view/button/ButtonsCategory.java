package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonsCategory {

    /**
     * Represents the category "Capitali".
     */
    CAPITALS("CAPITALI","Sai indicare la capitale di ciascun paese?","/images/capitali.jpg",ButtonType.CATEGORY),
    /**
     * Represents the category "Valute".
     */
    CURRENCIES("VALUTE","Sai indicare qual e' la valuta adottata da ciascun paese?","/images/valute.jpg",ButtonType.CATEGORY),
    /**
     * Represents the category "Typical dishes".
     */
    DISHES("CUCINA","Sai indicare i paesi in base alla bandiera nazionale?","/images/cucina.jpg",ButtonType.CATEGORY),
    /**
     * Represents the category "Bandiere".
     */
    FLAGS("BANDIERA","Sai indicare i paesi in base alla bandiera nazionale?","/images/bandiere.jpg",ButtonType.CATEGORY),
    /**
     * Represents the category "Monumenti".
     */
    MONUMENTS("MONUMENTI","Sai indicare dove si trovano questi famosi monumenti?","/images/monumenti.jpg",ButtonType.CATEGORY),
    /**
     * Represents the difficulty level "Facile".
     */
    EASY("FACILE","","",ButtonType.DIFFICULTY_LEVEL),
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIUM("MEDIO","","",ButtonType.DIFFICULTY_LEVEL),
    /**
     * Represents the difficulty level "Difficile".
     */
    HARD("DIFFICILE","","",ButtonType.DIFFICULTY_LEVEL),
    /**
     * Represents the game mode "Classica".
            */
    CLASSIC("CLASSICA","","",ButtonType.GAME_MODE),
    /**
     * Represents the game mode "Sfida".
     */
    CHALLENGE("SFIDA","","",ButtonType.GAME_MODE),
    /**
     * Represents the game mode "Allenamento".
     */
    TRAINING("ALLENAMENTO","","",ButtonType.GAME_MODE
    );
    private final String buttonText;
    private final String categoryDescription;
    private final String imagePath;
    private final ButtonType buttonType;


    ButtonsCategory(String buttonText, String categoryDescription, String imagePath, ButtonType buttonType) {
        this.buttonText = buttonText;
        this.categoryDescription = categoryDescription;
        this.imagePath = imagePath;
        this.buttonType = buttonType;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getImagePath() {
        return imagePath;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }
}
