package com.geoquiz.view.menu;

import com.geoquiz.view.button.*;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.*;

import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;
    private static final Color BUTTON_COLOR = Color.BLUE;
    private static final Color COLOR_TEXT_USER_LABEL = Color.BLACK;
    private static final double USER_LABEL_FONT = 40;
    private static final String TEXT_USER_LABEL = "USER: ";

    private final Pane panel = new Pane();
    private static ButtonsCategory selectedCategory;
    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        final Map<ButtonsCategory,MyButton> allButton = new HashMap<>();
        for(ButtonsCategory categoryButton : ButtonsCategory.values()){
            if(categoryButton.getButtonType().equals(ButtonType.CATEGORY)){
                allButton.put(categoryButton,MyButtonFactory.createMyButton(categoryButton.getButtonText(),CategoryScene.BUTTON_COLOR,CategoryScene.BUTTON_WIDTH));
            }
        }

        final MyButton back;

        final MyLabel userLabel = MyLabelFactory.createMyLabel(CategoryScene.TEXT_USER_LABEL + LoginMenuScene.getUsername(),
                                                                CategoryScene.COLOR_TEXT_USER_LABEL ,
                                                                CategoryScene.USER_LABEL_FONT);

        back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) allButton.get(ButtonsCategory.FLAGS),
                                    (Node) allButton.get(ButtonsCategory.CURRENCIES),
                                    (Node) allButton.get(ButtonsCategory.DISHES));
        hbox2.getChildren().addAll((Node) allButton.get(ButtonsCategory.MONUMENTS),
                                    (Node) allButton.get(ButtonsCategory.CAPITALS));
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            this.playClickIfNotDisabled();
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        allButton.entrySet().forEach(myButton -> {
            ((Node) myButton.getValue()).setOnMouseClicked(event -> {
                this.playClickIfNotDisabled();
                this.selectedCategory = myButton.getKey();
                mainStage.setScene(new ModeScene(mainStage));
            });
        });

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }
    private void playClickIfNotDisabled(){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }
    /**
     * @return category.
     */
    public static ButtonsCategory getCategoryPressed() {
        return Objects.requireNonNull(CategoryScene.selectedCategory);
    }
}
